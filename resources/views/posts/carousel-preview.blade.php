<div class="gallery-photos masonry">
    @foreach($post->photos->take(3) as $photo)
        <figure>
            <img src="{{ $photo->url }}" alt="" class="img-responsive">
            @if($loop->iteration === 3)
                <div class="overlay">{{ $post->photos->count() }} fotos</div>
            @endif
        </figure>
    @endforeach
</div>
