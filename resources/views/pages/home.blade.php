@extends('layouts.layout')

@section('content')

@if(isset($title))
    <section class="posts container">
        <div class="post w-image">
            <h3 class="text-center">{{ $title }}</h3>
        </div>
    </section>
@endif
<section class="posts container">

    @foreach($posts as $post)
    <article class="post w-image">
        @include($post->viewType('-preview'))
        <div class="content-post">
            @include('posts.header')
            <h1>{{ $post->title }}</h1>
            <div class="divider"></div>
            <p>{{ $post->excerpt }}</p>
            <footer class="container-flex space-between">
                <div class="read-more">
                    <a href="{{ route('posts.show', [$post->slug]) }}" class="text-uppercase c-green">Leer mas</a>
                </div>
                @include('posts.tags')
            </footer>
        </div>
    </article>
    @endforeach

</section>

{{ $posts->links() }}

@endsection

