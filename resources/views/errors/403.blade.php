@extends('admin.layouts.layout')

@section('content')
    <section class="pages container">
        <div class="page page-about">
            <h1 class="text-capitalize">No tienes permiso de acceso</h1>
            <div class="divider-2" style="margin: 35px;"></div>
            <p>Volver a <a href="{{ route('dashboard') }}">Volver a la pagina principal</a></p>
        </div>
    </section>

@endsection
