@extends('layouts.layout')

@section('content')
    <section class="pages container">
        <div class="page page-about">
            <h1 class="text-capitalize">Pagina no encontrada</h1>
            <div class="divider-2" style="margin: 35px;"></div>
            <p>Volver a <a href="{{ route('pages.home') }}">Volver a Inicio</a></p>
        </div>
    </section>

@endsection
