<?php
return [
    'login' => [
        'header' => 'Identificate para acceder a tu sesion',
        'remember' => 'Recordar',
        'forgot' => 'He olvidado mi contraseña',
        'button' => 'Acceder'
    ],
];
?>
