<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create(['name' => 'Admin']);
        $writerRole = Role::create(['name' => 'Writer']);

        $viewPostPermission = Permission::create(['name' => 'View posts']);
        $createPostPermission = Permission::create(['name' => 'Create posts']);
        $updatePostPermission = Permission::create(['name' => 'Update posts']);
        $deletePostPermission = Permission::create(['name' => 'Delete posts']);

        $viewUserPermission = Permission::create(['name' => 'View users']);
        $createUserPermission = Permission::create(['name' => 'Create users']);
        $updateUserPermission = Permission::create(['name' => 'Update users']);
        $deleteUserPermission = Permission::create(['name' => 'Delete users']);

        $admin = new User;
        $admin->name = 'Jorge';
        $admin->email = 'jorgicoor1998@gmail.com';
        $admin->password = 'pokemon12';
        $admin->save();

        $admin->assignRole($adminRole);

        $write = new User;
        $write->name = 'Pepe';
        $write->email = 'pepe@gmail.com';
        $write->password = 'daw_2019';
        $write->save();

        $write->assignRole($writerRole);


        $users = factory(User::class, 8)->make();

        $users->each(function($u) use($writerRole) { //Para cada usuario utilizamos el rol...
           $u->save(); //Creamos los usuarios
           $u->assignRole($writerRole); //Asignamos los roles a cada usuario
        });

    }
}
