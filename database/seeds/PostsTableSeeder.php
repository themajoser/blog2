<?php

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('public')->deleteDirectory('posts');

        $categories = Category::all();
        $categories->each(function($c) {
            $posts = factory(Post::class, 10)->make(); //Con make prepare la creacion de 10 elementos.
            $c->posts()->saveMany($posts); //Guarda y crea los posts relacionados con las categorias.
            $posts->each(function($p) {
                $p->slug = Str::slug($p->title);
                $p->save();
                $tags = Tag::all()->pluck('id')->toArray();
                $p->tags()->attach(Arr::random($tags, 2));
            });
        }); //each() metodo de iteracion para cada dato de la tabla

    }
}


















