<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
        $category = new Category;
        $category->name = 'Categoria_1';
        $category->save();

        $category = new Category;
        $category->name = 'Categoria_2';
        $category->save();
         **/

        factory(Category::class, 10)->create();

    }
}
