<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => rtrim($faker->sentence(2, 'true'), '.') //word() recibe un argumento que indica que tiene que tener al menos 3 palabras
    ];
});
