<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title'); //Titulo
            $table->string('slug')->nullable();
            $table->mediumText('excerpt')->nullable(); //Resumen o estracto
            $table->mediumText('iframe')->nullable();
            $table->text('body')->nullable(); //Cuerpo de texto
            $table->integer('visitas')->nullable()->default(0);
            $table->timestamp('published_at')->nullable(); //nullable() inserta el valor por defecto como null.
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->on('categories')->references('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
