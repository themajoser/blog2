<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     * Aqui donde vamos a registrar el evento UserwasCreated, se suele llamar en pasado, ocurre cuando algo ha pasado
     * Tiene asociado un único listener el de sendlogincredentials
     * Cada vez un usuario se eejcuta todos los listener que haya dentro
     * art event:generate
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendLoginCredentials::class,
        ],
        'App\Events\UserWasCreated'=> [
            'App\Listeners\SendLoginCredentials',
        ],
        'App\Events\UserVisitPost'=>[
            'App\Listeneres\RegisterVisit',
            ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
