<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'iframe', 'published_at', 'category_id', 'excerpt', 'user_id','visitas'];
    protected $dates = ['published_at'];
    use SoftDeletes;
    public static function boot()
    {
        parent::boot();

        static::deleting(function($post) {
            $post->photos->each->delete();
            $post->tags()->detach(); //Borramos las etiquetas
        });

    }

    public static function create(array $attributes = []) //Sobreescribimos el metodo create().
    {
        $attributes['user_id'] = auth()->user()->id;
        $post = static::query()->create($attributes); //Genera un consulta para crear datos con los atributos que esta recibiendo.

        $post->generateSlug();

        return $post;
    }

    public function generateSlug()
    {
        $slug = Str::slug($this->title);

        if(static::whereSlug($slug)->exists()) {
            $slug .= '-' . $this->id;
        }

        //$this->slug = static::where('slug', $slug)->exits() ? '-' . $post->id : $slug;

        $this->slug = $slug;
        $this->save();
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Comment::class); //Solo obtiene un dato (1,1)
    }
    public function comments()
    {
        return $this->hasMany(Comment::class); //Solo obtiene un dato (1,1)
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function scopePublished($query)
    {
        $query->whereNotNull('published_at')  //Metodo que comprueba si el dato no es null.
        ->where('published_at', '<', Carbon::now())
            ->latest('published_at');
    }

    public function scopeAllowed($query)
    {
        if(auth()->user()->can('view', $this)) { //Si el usuario puede ver los post...
            return $query;
        }else {
            return $query->where('user_id', auth()->id());
        }
    }
    public function registrarVisita(){
        $this->visitas+=1;
        $this->save();
    }

    public function isPublished()
    {
        return ! is_null($this->published_at) && $this->published_at < today();
    }


    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = $published_at ? Carbon::parse($published_at) : null;
    }

    public function setCategoryIdAttribute($category_id)
    {
        $this->attributes['category_id'] = Category::find($category_id) ? $category_id : Category::create(['name' => $category_id])->id;
    }

    public function syncTags($tags)
    {
        $tagIds = collect($tags)->map(function($tag) {
            return Tag::find($tag) ? $tag : Tag::create(['name' => $tag])->id;
        }); //Convertimos en una coleccion y generamos un mapa para recorrer la coleccion.

        return $this->tags()->sync($tagIds);
    }


    public function viewType($view = '')
    {
        if($this->photos->count() === 1) {
            return 'posts.photo';
        }elseif($this->photos->count() > 1) {
            return 'posts.carousel' . $view;
        }elseif($this->iframe) {
            return 'posts.iframe';
        }else{
            return 'posts.text';
        }
    }
}
