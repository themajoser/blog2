<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Comment extends Model
{
    protected $fillable = ['comentario','post_id','user_id'];

    public function posts()
    {
        return $this->belongsTo(User::class);
    }


}
