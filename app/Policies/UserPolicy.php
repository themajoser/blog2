<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($auth_user)
    {
        return $auth_user->hasRole('Admin') ? true : null;
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $auth_user
     * @return mixed
     */
    public function viewAny(User $auth_user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     * El primero es el autentificado , el segundo a quien le queramos dar permiso
     * @param  \App\User  $auth_user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $auth_user, User $model)
    {
        return $auth_user->id == $model->id || $auth_user->hasPermissionTo('View users');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $auth_user
     * @return mixed
     */
    public function create(User $auth_user)
    {
        return $auth_user->hasPermissionTo('Create users');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $auth_user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $auth_user, User $model)
    {
        return $auth_user->id == $model->id || $auth_user->hasPermissionTo('View users');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $auth_user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $auth_user, User $model)
    {
        return $auth_user->id == $model->id || $auth_user->hasPermissionTo('View users');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $auth_user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $auth_user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $auth_user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $auth_user, User $model)
    {
        //
    }
}
