<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use App\Mail\LoginCredentials;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendLoginCredentials
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    //Siempre recibe un objeto del evento , por eso ponemos las propiedades como públicas
    //Kiuen es para ponerlo en cola, se lo delega a otro hilo, o sino tendrá que esperar hasta que no finalice el proceso
    //art make:mail LoginCredentials -m emails.login-credentials
    //Este método tendremos que decirle cuando se llame al listener, recibe al evento
    public function handle(UserWasCreated $event)
    {
        Mail::to($event->user)->queue(
            new LoginCredentials($event->user,$event->password)
        );
    }
}
