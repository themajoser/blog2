<?php

namespace App\Listeneres;

use App\Events\UserVisitPost;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterVisit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserVisitPost  $event
     * @return void
     */
    public function handle(UserVisitPost $event)
    {
        $event->post->visitas+=1;
        $event->post->save();
    }
}
