<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    protected $fillable = ['post_id', 'url'];

    public static function boot()
    {
        parent::boot();

        //metodo estatica que realiza un borrado de un fichero tanto en el modelo como fisico
        static::deleting(function ($photo){
            Storage::disk('public')->delete($photo->url);
        });
    }
}
