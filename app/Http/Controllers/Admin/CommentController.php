<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{


    public function store(Request $request, Post $post)
    {
        $this->validate($request, [
            'comentario' => 'required',
        ]);
         @auth()->user()->id;
        Comment::create([
            'comentario' => $request['comentario'],
            'post_id' => $post->id,
            'user_id'=>auth()->user()->id
        ]);

        return redirect()
            ->route('posts.show', $post);

    }
    public function show(Post $post)
    {

    }




    public function update(StorePostRequest $request, Post $post)
    {

        $post->update($request->all());

        $post->syncTags($request->tags);

        return redirect()->route('admin.comments.edit', $post)->with('flash', 'El post ha sido actualizado correctamente');
    }

    public function destroy(Post $post)
    {


        $post->delete();

        return redirect()->route('admin.comments.index')->with('flash', 'El post ha sido borrado correctamente');
    }
}
