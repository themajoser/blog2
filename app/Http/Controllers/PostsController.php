<?php

namespace App\Http\Controllers;

use App\Events\UserVisitPost;
use App\Events\UserWasCreated;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show(Post $post)
    {

            $comments = $post->comments;
            if ($post->isPublished() || auth()->check()) {
                return view('posts.show', compact('post','comments'));
            }
            abort(404);
    }

}
