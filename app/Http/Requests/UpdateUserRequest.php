<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                 Rule::unique('users')->ignore($this->route('user')->id)
            ]
        ];

        if($this->filled('password')) { //Indica si es campo se ha insertado valor.
            $rules['password'] = 'confirmed | min: 6'; //Define las reglas como un array asociativo. Es equivalente a $this->validate.
        }

        return $rules;
    }
}
