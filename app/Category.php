<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ['name'];
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getRouteKeyName()
    {
        return 'url';
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name; //Obtiene el valor del campo name.
        $this->attributes['url'] = Str::slug($name); //Obtiene automatica el valor del campo url una vez obtenido el valor del name.
    }
}

